/*
Navicat MySQL Data Transfer

Source Server         : mysq on localhost
Source Server Version : 50611
Source Host           : localhost:3306
Source Database       : bhaktikaryamuda

Target Server Type    : MYSQL
Target Server Version : 50611
File Encoding         : 65001

Date: 2018-03-27 16:13:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `level` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES ('0', 'duwi haryanto', 'duwi', 'admin', '1');

-- ----------------------------
-- Table structure for anggota
-- ----------------------------
DROP TABLE IF EXISTS `anggota`;
CREATE TABLE `anggota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of anggota
-- ----------------------------
INSERT INTO `anggota` VALUES ('1', '16.02.2528', 'danang yuli A', null, null, '085725818424', null, 'aktif');
INSERT INTO `anggota` VALUES ('2', '16.07.2520', 'andaru sayekti', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('3', '16.10.2510', 'estu prasetya', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('4', '16.06.2531', 'roihana kurnia D', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('5', '16.01.2507', 'lia alviani', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('6', '16.03.2509', 'intan herdiana', null, null, null, null, 'nonaktif');
INSERT INTO `anggota` VALUES ('7', '16.02.2508', 'bono sumarjiyanto', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('8', '16.08.2519', 'tika dewi kasturi', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('9', '16.04.2505', 'sri rahayu', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('10', '16.07.2518', 'irwan yunianto', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('11', '16.08.2530', 'febriana ika kusuma', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('12', '16.09.2504', 'ika setyaningrum', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('13', '16.01.2534', 'dimas satriya', null, null, null, null, 'nonaktif');
INSERT INTO `anggota` VALUES ('14', '16.08.2523', 'tri vira', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('15', '16.07.2521', 'ferdian nurcahya s', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('16', '16.01.2516', 'iga mawarti', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('17', '16.06.2514', 'nurochman abadi', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('18', '16.10.2502', 'apriliana kurniawati', null, null, null, null, 'nonaktif');
INSERT INTO `anggota` VALUES ('19', '16.06.2532', 'nursapti', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('20', '16.07.2527', 'm rafif naufal', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('21', '16.10.2506', 'yuni asih', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('22', '16.02.2533', 'yogi sabena', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('23', '16.07.2526', 'isti rosidah', null, null, null, null, 'aktif');
INSERT INTO `anggota` VALUES ('24', '16.07.2501', 'duwi haryanto', null, null, null, null, 'aktif');

-- ----------------------------
-- Table structure for anggota_temp
-- ----------------------------
DROP TABLE IF EXISTS `anggota_temp`;
CREATE TABLE `anggota_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(30) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `pedukuhan` varchar(255) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of anggota_temp
-- ----------------------------
INSERT INTO `anggota_temp` VALUES ('34', null, 'duwi haryanto', 'laki-laki', '2017-07-02', 'haryanto.duwi@gmail.com', '08572518424', 'Pedak', 'nonaktif');
INSERT INTO `anggota_temp` VALUES ('38', null, 'ariyanto haryanto', 'laki-laki', '2017-07-03', 'ariyanto@gmail.com', '0857254542424', 'Kauman', 'aktif');
INSERT INTO `anggota_temp` VALUES ('40', null, 'kindani kiromi', 'laki-laki', '2017-07-03', 'jerry.rhamadoni@yahoo.com', '08572518424', 'Pandak', 'aktif');
INSERT INTO `anggota_temp` VALUES ('66', null, 'mahesaa', null, null, 'mahesa@gmail.com', null, null, null);
INSERT INTO `anggota_temp` VALUES ('70', null, 'arman', null, null, 'arman@gmail.com', null, null, null);
INSERT INTO `anggota_temp` VALUES ('71', null, 'arman', null, null, 'arman@gmail.com', null, null, null);
INSERT INTO `anggota_temp` VALUES ('72', null, 'arman', null, null, 'arman@gmail.com', null, null, null);
INSERT INTO `anggota_temp` VALUES ('73', null, 'kilua', null, null, 'kilua@gmail.com', null, null, null);
INSERT INTO `anggota_temp` VALUES ('75', null, 'gon', null, null, 'gon@gmail.com', null, null, null);
INSERT INTO `anggota_temp` VALUES ('76', null, 'sifa', null, null, null, null, null, null);
INSERT INTO `anggota_temp` VALUES ('77', null, 'Haryanto Stark', null, null, 'strak.haryanto@gmail.com', '085725818424', null, null);
INSERT INTO `anggota_temp` VALUES ('78', null, 'Haryanto Stark', null, null, 'strak.haryanto@gmail.com', '085725818424', null, null);

-- ----------------------------
-- Table structure for upload
-- ----------------------------
DROP TABLE IF EXISTS `upload`;
CREATE TABLE `upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploader` text,
  `raport` text,
  `skhun` text,
  `ktp` text,
  `bebasnarkoba` text,
  `bebasbutawarna` text,
  `piagam` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of upload
-- ----------------------------
INSERT INTO `upload` VALUES ('9', 'test 3', 'c3facf6883e9f274e233627bcf993ed2.pdf', '406a2043c82fc9ac67fa2f12d3bdfa4d.pdf', 'ba0dff607bf8e557bb0490393ed63e72.pdf', '7af11c9755b382d1919cb98789de8c93.pdf', '', '');
INSERT INTO `upload` VALUES ('10', 'test 4', '194f2081e11279bee8a440f03b2e582f.pdf', 'a288c21c53c0b491e5edf6b8bb11e4bf.pdf', 'dfbbc015850418988b88ed18b6e49406.pdf', '2024eed5e5d78715f42cbec01e02361a.pdf', '50649a2e227640d9028133327c0d843b.pdf', '57a958722ba3ad7527e547f4ae82327f.pdf');
INSERT INTO `upload` VALUES ('11', 'test 5', 'c4e7076b58d193ac3f6b013c5c242e12.pdf', 'c4e7076b58d193ac3f6b013c5c242e12.pdf', '7717455a1c0357b5e763fd2e7cf00578.pdf', 'a8e99b9373b01c32ffe734f1ca9c4113.pdf', 'c232a877f0cadc09043f40039e48797a.pdf', '3f01c7d8f44145777da524859662acdd.pdf');
