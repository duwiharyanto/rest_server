<?php
Class Client extends CI_Controller{
    
    var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API="http://localhost/rest";
    }
    
    // menampilkan data mahasiswa
    function index(){
        $data['mahasiswa'] = json_decode($this->curl->simple_get($this->API.'/data?key-id=123456'));
        //$this->load->view('mahasiswa/list',$data);
        if(is_array($data['mahasiswa'])){
            print_r($data['mahasiswa']);
        }else{
            echo "Koneksi Gagal";
        }
        
    }
    
    // insert data mahasiswa
    function simpan(){
        // if(isset($_POST['submit'])){
        //     $data = array(
        //         'nama'=>"Mark Markuest",
        //         'jenis_kelamin'=>"Laki-laki",
        //         'email'=>"mark@gmail.com",
        //         'no_hp'=>"085725818424"
        //     );
        //     $insert =  $this->curl->simple_post($this->API.'/data', $data, array(CURLOPT_BUFFERSIZE => 10)); 
        //     if($insert)
        //     {
        //         $this->session->set_flashdata('hasil','Insert Data Berhasil');
        //     }else
        //     {
        //        $this->session->set_flashdata('hasil','Insert Data Gagal');
        //     }
        //     redirect('mahasiswa');
        // }else{
        //     $data['jurusan'] = json_decode($this->curl->simple_get($this->API.'/jurusan'));
        //     $this->load->view('mahasiswa/create',$data);
        // }
        $data = array(
            'nama'=>"Mark Markuest",
            'jenis_kelamin'=>"Laki-laki",
            'email'=>"mark@gmail.com",
            'no_hp'=>"085725818424"
        ); 
        $insert =  $this->curl->simple_post($this->API.'/data', $data, array(CURLOPT_BUFFERSIZE => 10)); 
        if($insert){
            echo "Data Berhasil Disimpan";
        }else{
            echo "Data Gagal Disimpan";
        }      
    }
    

    // edit data mahasiswa
    function edit(){
        $data=array('id'=>$this->uri->segment(3));
        $data['data']=json_decode($this->curl->simple_get($this->API.'/data',$data));
        print_r($data['data']);
    }
    function update(){
        // if(isset($_POST['submit'])){
        //     $data = array(
        //         'nim'       =>  $this->input->post('nim'),
        //         'nama'      =>  $this->input->post('nama'),
        //         'id_jurusan'=>  $this->input->post('jurusan'),
        //         'alamat'    =>  $this->input->post('alamat'));
        //     $update =  $this->curl->simple_put($this->API.'/mahasiswa', $data, array(CURLOPT_BUFFERSIZE => 10)); 
        //     if($update)
        //     {
        //         $this->session->set_flashdata('hasil','Update Data Berhasil');
        //     }else
        //     {
        //        $this->session->set_flashdata('hasil','Update Data Gagal');
        //     }
        //     redirect('mahasiswa');
        // }else{
        //     $data['jurusan'] = json_decode($this->curl->simple_get($this->API.'/jurusan'));
        //     $params = array('nim'=>  $this->uri->segment(3));
        //     $data['mahasiswa'] = json_decode($this->curl->simple_get($this->API.'/mahasiswa',$params));
        //     $this->load->view('mahasiswa/edit',$data);
        // }
        $data=array(
            'id'=>"79",
            'nama'=>"Daniel Pedrosaa",
            'jenis_kelamin'=>"Perempuan",
            'email'=>"daniel@gmail.com",
            'no_hp'=>"085725844",
            );
        $update = $this->curl->simple_put($this->API.'/data', $data, array(CURLOPT_BUFFERSIZE => 10)); 
        if($update){
            echo "Update Berhasil";
        }else{
            echo "Update Data Gagal";
        }
    }
    
    // // delete data mahasiswa
    function delete(){
        // if(empty($nim)){
        //     redirect('mahasiswa');
        // }else{
        //     $delete =  $this->curl->simple_delete($this->API.'/mahasiswa', array('nim'=>$nim), array(CURLOPT_BUFFERSIZE => 10)); 
        //     if($delete)
        //     {
        //         $this->session->set_flashdata('hasil','Delete Data Berhasil');
        //     }else
        //     {
        //        $this->session->set_flashdata('hasil','Delete Data Gagal');
        //     }
        //     redirect('mahasiswa');
        // }
        $data=array('id'=>$this->uri->segment(3));
        $delete = $this->curl->simple_delete($this->API.'/data', $data, array(CURLOPT_BUFFERSIZE => 10)); 
        if($delete)
        {
            //$this->session->set_flashdata('hasil','Delete Data Berhasil');
            echo "Hapus berhasil";
            //echo $id;
        }else
        {
           //$this->session->set_flashdata('hasil','Delete Data Gagal');
            echo "Hapus data gagal";
        }        
    }
}